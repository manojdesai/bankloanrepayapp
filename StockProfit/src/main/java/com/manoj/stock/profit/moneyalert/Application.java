package com.manoj.stock.profit.moneyalert;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

public class Application {
	private String code;
	private String message;

	private Data DataObject;

	
	public String getCode() {
		return code;
	}

	public String getMessage() {
		return message;
	}

	@JsonProperty("data")
	public Data getData() {
		return DataObject;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	
	public void setData(Data dataObject) {
		this.DataObject = dataObject;
	}
}