package com.manoj.stock.profit.moneyalert;

import com.fasterxml.jackson.annotation.JsonGetter;

public class Data {
	
	private double LP;


	private double HP;

	private double lower_circuit_limit;
	private double upper_circuit_limit;

	private String ty;
	private String HN;

	private String DISPID;
	private double IND_PE;
	private double BIDQ;
	private double OPN;
	private double BIDP;
	private double DVolAvg5;

	private double sc_ttm_cons;
	private double SC_TTM;
	private double AvgDelVolPer_5day;

	private String DTTIME;
	private String SC_FULLNM;

	private float P_C;
	private float etf;
	private String isinid;
	private String YR;
	private String SC_SUBSEC;

	private double VOL;

	private String exchange;
	private String sc_mapindex;
	private double DVolAvg30;
	private float tot_buy_qty;
	private double AVGP;
	private String Group;
	private String LTH = null;
	private double ACCOD;
	private String LTL = null;
	private String NSEID;

	private double _52H;
	private double _52L;
	private double _5DayAvg;
	private double _30DayAvg;
	private double _50DayAvg;
	

	
	private double _150DayAvg;
	private double _200DayAvg;
	private double AvgDelVolPer_8day;
	private double FV;
	private float MKTCAP;
	private String SMID;
	private double DIVPR;
	private float tot_sell_qty;

	private double DELV;
	private double BV;
	private String PREVDATE;
	private String MKTLOT;

	private double OFFERP;
	private double OFFERQ;
	private String lastupd;
	private String SERIES;
	private String BSEID;
	private String GN;

	private double AvgDelVolPer_3day;
	private float LDAYS;
	private double TID;
	private String MKT_LOT;
	private double SHRS;

	private float PE;
	private String TVOL;
	private String DISPTYP;
	private String im_scid;
	private double CEPS;
	private double DVolAvg10;
	private double pricecurrent;
	private double priceprevclose;
	private String symbol;
	private String company;
	private double pricechange;
	private double pricepercentchange;
	private String lastupd_epoch;

	@JsonGetter("LP")
	public double getLP() {
		return LP;
	}

	@JsonGetter("HP")
	public double getHP() {
		return HP;
	}

	public double getLower_circuit_limit() {
		return lower_circuit_limit;
	}

	public double getUpper_circuit_limit() {
		return upper_circuit_limit;
	}

	public String getTy() {
		return ty;
	}

	@JsonGetter("HN")
	public String getHN() {
		return HN;
	}

	@JsonGetter("DISPID")
	public String getDISPID() {
		return DISPID;
	}

	@JsonGetter("IND_PE")
	public double getIND_PE() {
		return IND_PE;
	}

	@JsonGetter("BIDQ")
	public double getBIDQ() {
		return BIDQ;
	}

	@JsonGetter("OPN")
	public double getOPN() {
		return OPN;
	}

	@JsonGetter("BIDP")
	public double getBIDP() {
		return BIDP;
	}

	@JsonGetter("DTTIME")
	public String getDTTIME() {
		return DTTIME;
	}
	@JsonGetter("SC_FULLNM")
	public String getSC_FULLNM() {
		return SC_FULLNM;
	}

	@JsonGetter("DVolAvg5")
	public double getDVolAvg5() {
		return DVolAvg5;
	}

	@JsonGetter("P_C")
	public float getP_C() {
		return P_C;
	}

	@JsonGetter("etf")
	public float getEtf() {
		return etf;
	}

	@JsonGetter("isinid")
	public String getIsinid() {
		return isinid;
	}

	@JsonGetter("YR")
	public String getYR() {
		return YR;
	}

	@JsonGetter("SC_SUBSEC")
	public String getSC_SUBSEC() {
		return SC_SUBSEC;
	}

	@JsonGetter("sc_ttm_cons")
	public double getSc_ttm_cons() {
		return sc_ttm_cons;
	}

	@JsonGetter("SC_TTM")
	public double getSC_TTM() {
		return SC_TTM;
	}

	@JsonGetter("AvgDelVolPer_5day")
	public double getAvgDelVolPer_5day() {
		return AvgDelVolPer_5day;
	}

	@JsonGetter("VOL")
	public double getVOL() {
		return VOL;
	}

	@JsonGetter("exchange")
	public String getExchange() {
		return exchange;
	}

	@JsonGetter("sc_mapindex")
	public String getSc_mapindex() {
		return sc_mapindex;
	}

	@JsonGetter("DVolAvg30")
	public double getDVolAvg30() {
		return DVolAvg30;
	}

	@JsonGetter("tot_buy_qty")
	public float getTot_buy_qty() {
		return tot_buy_qty;
	}

	@JsonGetter("AVGP")
	public double getAVGP() {
		return AVGP;
	}

	@JsonGetter("Group")
	public String getGroup() {
		return Group;
	}

	@JsonGetter("LTH")
	public String getLTH() {
		return LTH;
	}

	@JsonGetter("ACCOD")
	public double getACCOD() {
		return ACCOD;
	}

	@JsonGetter("LTL")
	public String getLTL() {
		return LTL;
	}

	@JsonGetter("NSEID")
	public String getNSEID() {
		return NSEID;
	}

	@JsonGetter("52L")
	public double get52L() {
		return _52L;
	}

	@JsonGetter("52H")
	public double get52H() {
		return _52H;
	}

	@JsonGetter("AvgDelVolPer_8day")
	public double getAvgDelVolPer_8day() {
		return AvgDelVolPer_8day;
	}
	
	@JsonGetter("FV")
	public double getFV() {
		return FV;
	}

	@JsonGetter("MKTCAP")
	public float getMKTCAP() {
		return MKTCAP;
	}
	
	@JsonGetter("SMID")
	public String getSMID() {
		return SMID;
	}
	
	@JsonGetter("DIVPR")
	public double getDIVPR() {
		return DIVPR;
	}

	@JsonGetter("tot_sell_qty")
	public float getTot_sell_qty() {
		return tot_sell_qty;
	}

	@JsonGetter("DELV")
	public double getDELV() {
		return DELV;
	}

	@JsonGetter("BV")
	public double getBV() {
		return BV;
	}

	@JsonGetter("PREVDATE")
	public String getPREVDATE() {
		return PREVDATE;
	}

	@JsonGetter("MKTLOT")
	public String getMKTLOT() {
		return MKTLOT;
	}

	@JsonGetter("OFFERP")
	public double getOFFERP() {
		return OFFERP;
	}

	@JsonGetter("OFFERQ")
	public double getOFFERQ() {
		return OFFERQ;
	}

	@JsonGetter("lastupd")
	public String getLastupd() {
		return lastupd;
	}

	@JsonGetter("SERIES")
	public String getSERIES() {
		return SERIES;
	}

	@JsonGetter("BSEID")
	public String getBSEID() {
		return BSEID;
	}

	@JsonGetter("GN")
	public String getGN() {
		return GN;
	}

	@JsonGetter("AvgDelVolPer_3day")
	public double getAvgDelVolPer_3day() {
		return AvgDelVolPer_3day;
	}

	@JsonGetter("LDAYS")
	public float getLDAYS() {
		return LDAYS;
	}

	@JsonGetter("TID")
	public double getTID() {
		return TID;
	}

	@JsonGetter("MKT_LOT")
	public String getMKT_LOT() {
		return MKT_LOT;
	}

	@JsonGetter("SHRS")
	public double getSHRS() {
		return SHRS;
	}

	@JsonGetter("5DayAvg")
	public double get5DayAvg() {
		return _5DayAvg;
	}

	@JsonGetter("30DayAvg")
	public double get30DayAvg() {
		return _30DayAvg;
	}

	@JsonGetter("50DayAvg")
	public double get50DayAvg() {
		return _50DayAvg;
	}



	@JsonGetter("150DayAvg")
	public double get150DayAvg() {
		return _150DayAvg;
	}

	
	@JsonGetter("200DayAvg")
	public double get200DayAvg() {
		return this._200DayAvg;
	}

	@JsonGetter("PE")
	public float getPE() {
		return PE;
	}

	@JsonGetter("TVOL")
	public String getTVOL() {
		return TVOL;
	}

	
	@JsonGetter("DISPTYP")
	public String getDISPTYP() {
		return DISPTYP;
	}

	@JsonGetter("im_scid")
	public String getIm_scid() {
		return im_scid;
	}

	@JsonGetter("CEPS")
	public double getCEPS() {
		return CEPS;
	}

	@JsonGetter("DVolAvg10")
	public double getDVolAvg10() {
		return DVolAvg10;
	}

	
	@JsonGetter("pricecurrent")
	public double getPricecurrent() {
		return pricecurrent;
	}

	@JsonGetter("priceprevclose")
	public double getPriceprevclose() {
		return priceprevclose;
	}

	@JsonGetter("symbol")
	public String getSymbol() {
		return symbol;
	}

	@JsonGetter("company")
	public String getCompany() {
		return company;
	}

	@JsonGetter("pricechange")
	public double getPricechange() {
		return pricechange;
	}

	@JsonGetter("pricepercentchange")
	public double getPricepercentchange() {
		return pricepercentchange;
	}

	@JsonGetter("lastupd_epoch")
	public String getLastupd_epoch() {
		return lastupd_epoch;
	}

	
	public void setLP(double LP) {
		this.LP = LP;
	}


	public void setLower_circuit_limit(double lower_circuit_limit) {
		this.lower_circuit_limit = lower_circuit_limit;
	}

	public void setUpper_circuit_limit(double upper_circuit_limit) {
		this.upper_circuit_limit = upper_circuit_limit;
	}

	public void setTy(String ty) {
		this.ty = ty;
	}

	public void setHN(String HN) {
		this.HN = HN;
	}

	public void setHP(double HP) {
		this.HP = HP;
	}

	public void setDISPID(String DISPID) {
		this.DISPID = DISPID;
	}

	public void setIND_PE(double IND_PE) {
		this.IND_PE = IND_PE;
	}

	public void setBIDQ(double BIDQ) {
		this.BIDQ = BIDQ;
	}

	public void setOPN(double OPN) {
		this.OPN = OPN;
	}

	public void setBIDP(double BIDP) {
		this.BIDP = BIDP;
	}

	public void setDTTIME(String DTTIME) {
		this.DTTIME = DTTIME;
	}

	public void setSC_FULLNM(String SC_FULLNM) {
		this.SC_FULLNM = SC_FULLNM;
	}

	public void setDVolAvg5(double DVolAvg5) {
		this.DVolAvg5 = DVolAvg5;
	}

	public void setP_C(float P_C) {
		this.P_C = P_C;
	}

	public void setEtf(float etf) {
		this.etf = etf;
	}

	public void setIsinid(String isinid) {
		this.isinid = isinid;
	}

	public void setYR(String YR) {
		this.YR = YR;
	}

	public void setSC_SUBSEC(String SC_SUBSEC) {
		this.SC_SUBSEC = SC_SUBSEC;
	}

	public void setSc_ttm_cons(double sc_ttm_cons) {
		this.sc_ttm_cons = sc_ttm_cons;
	}

	public void setSC_TTM(double SC_TTM) {
		this.SC_TTM = SC_TTM;
	}

	public void setAvgDelVolPer_5day(double AvgDelVolPer_5day) {
		this.AvgDelVolPer_5day = AvgDelVolPer_5day;
	}

	public void setVOL(double VOL) {
		this.VOL = VOL;
	}

	public void set5DayAvg(double _5DayAvg) {
		this._5DayAvg = _5DayAvg;
	}

	public void set30DayAvg(double _30DayAvg) {
		this._30DayAvg = _30DayAvg;
	}

	public void set50DayAvg(double _50DayAvg) {
		this._50DayAvg = _50DayAvg;
	}


	public void set150DayAvg(double _150DayAvg) {
		this._150DayAvg = _150DayAvg;
	}

	public void set200DayAvg(double _200DayAvg) {
		this._200DayAvg = _200DayAvg;
	}

	public void setExchange(String exchange) {
		this.exchange = exchange;
	}

	public void setSc_mapindex(String sc_mapindex) {
		this.sc_mapindex = sc_mapindex;
	}

	public void setDVolAvg30(double DVolAvg30) {
		this.DVolAvg30 = DVolAvg30;
	}

	public void setTot_buy_qty(float tot_buy_qty) {
		this.tot_buy_qty = tot_buy_qty;
	}

	public void setAVGP(double AVGP) {
		this.AVGP = AVGP;
	}

	public void setGroup(String Group) {
		this.Group = Group;
	}

	public void setLTH(String LTH) {
		this.LTH = LTH;
	}

	public void setACCOD(double ACCOD) {
		this.ACCOD = ACCOD;
	}

	public void setLTL(String LTL) {
		this.LTL = LTL;
	}

	public void setNSEID(String NSEID) {
		this.NSEID = NSEID;
	}

	public void set52H(double _52H) {
		this._52H = _52H;
	}

	public void set52L(double _52L) {
		this._52L = _52L;
	}

	public void setAvgDelVolPer_8day(double AvgDelVolPer_8day) {
		this.AvgDelVolPer_8day = AvgDelVolPer_8day;
	}

	public void setFV(double FV) {
		this.FV = FV;
	}

	public void setMKTCAP(float MKTCAP) {
		this.MKTCAP = MKTCAP;
	}

	public void setSMID(String SMID) {
		this.SMID = SMID;
	}

	public void setDIVPR(double DIVPR) {
		this.DIVPR = DIVPR;
	}

	public void setTot_sell_qty(float tot_sell_qty) {
		this.tot_sell_qty = tot_sell_qty;
	}

	public void setDELV(double DELV) {
		this.DELV = DELV;
	}

	public void setBV(double BV) {
		this.BV = BV;
	}

	public void setPREVDATE(String PREVDATE) {
		this.PREVDATE = PREVDATE;
	}

	public void setMKTLOT(String MKTLOT) {
		this.MKTLOT = MKTLOT;
	}

	public void setOFFERP(double OFFERP) {
		this.OFFERP = OFFERP;
	}

	public void setOFFERQ(double OFFERQ) {
		this.OFFERQ = OFFERQ;
	}

	public void setLastupd(String lastupd) {
		this.lastupd = lastupd;
	}

	public void setSERIES(String SERIES) {
		this.SERIES = SERIES;
	}

	public void setBSEID(String BSEID) {
		this.BSEID = BSEID;
	}

	public void setGN(String GN) {
		this.GN = GN;
	}

	public void setAvgDelVolPer_3day(double AvgDelVolPer_3day) {
		this.AvgDelVolPer_3day = AvgDelVolPer_3day;
	}

	public void setLDAYS(float LDAYS) {
		this.LDAYS = LDAYS;
	}

	public void setTID(double TID) {
		this.TID = TID;
	}

	public void setMKT_LOT(String MKT_LOT) {
		this.MKT_LOT = MKT_LOT;
	}

	public void setSHRS(double SHRS) {
		this.SHRS = SHRS;
	}

	public void setPE(float PE) {
		this.PE = PE;
	}

	public void setTVOL(String TVOL) {
		this.TVOL = TVOL;
	}

	public void setDISPTYP(String DISPTYP) {
		this.DISPTYP = DISPTYP;
	}

	public void setIm_scid(String im_scid) {
		this.im_scid = im_scid;
	}

	public void setCEPS(double CEPS) {
		this.CEPS = CEPS;
	}

	public void setDVolAvg10(double DVolAvg10) {
		this.DVolAvg10 = DVolAvg10;
	}

	public void setPricecurrent(double pricecurrent) {
		this.pricecurrent = pricecurrent;
	}

	public void setPriceprevclose(double priceprevclose) {
		this.priceprevclose = priceprevclose;
	}

	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public void setPricechange(double pricechange) {
		this.pricechange = pricechange;
	}

	public void setPricepercentchange(double pricepercentchange) {
		this.pricepercentchange = pricepercentchange;
	}

	public void setLastupd_epoch(String lastupd_epoch) {
		this.lastupd_epoch = lastupd_epoch;
	}
	
	
	
	@Override
	public String toString() {
		return "Data [LP=" + LP + ", HP=" + HP + ", lower_circuit_limit=" + lower_circuit_limit
				+ ", upper_circuit_limit=" + upper_circuit_limit + ", ty=" + ty + ", HN=" + HN + ", DISPID=" + DISPID
				+ ", IND_PE=" + IND_PE + ", BIDQ=" + BIDQ + ", OPN=" + OPN + ", BIDP=" + BIDP + ", DVolAvg5=" + DVolAvg5
				+ ", sc_ttm_cons=" + sc_ttm_cons + ", SC_TTM=" + SC_TTM + ", AvgDelVolPer_5day=" + AvgDelVolPer_5day
				+ ", DTTIME=" + DTTIME + ", SC_FULLNM=" + SC_FULLNM + ", P_C=" + P_C + ", etf=" + etf + ", isinid="
				+ isinid + ", YR=" + YR + ", SC_SUBSEC=" + SC_SUBSEC + ", VOL=" + VOL + ", exchange=" + exchange
				+ ", sc_mapindex=" + sc_mapindex + ", DVolAvg30=" + DVolAvg30 + ", tot_buy_qty=" + tot_buy_qty
				+ ", AVGP=" + AVGP + ", Group=" + Group + ", LTH=" + LTH + ", ACCOD=" + ACCOD + ", LTL=" + LTL
				+ ", NSEID=" + NSEID + ", _52H=" + _52H + ", _52L=" + _52L + ", _5DayAvg=" + _5DayAvg + ", _30DayAvg="
				+ _30DayAvg + ", _50DayAvg=" + _50DayAvg + ", _150DayAvg=" + _150DayAvg
				+ ", _200DayAvg=" + _200DayAvg + ", AvgDelVolPer_8day=" + AvgDelVolPer_8day + ", FV=" + FV + ", MKTCAP="
				+ MKTCAP + ", SMID=" + SMID + ", DIVPR=" + DIVPR + ", tot_sell_qty=" + tot_sell_qty + ", DELV=" + DELV
				+ ", BV=" + BV + ", PREVDATE=" + PREVDATE + ", MKTLOT=" + MKTLOT + ", OFFERP=" + OFFERP + ", OFFERQ="
				+ OFFERQ + ", lastupd=" + lastupd + ", SERIES=" + SERIES + ", BSEID=" + BSEID + ", GN=" + GN
				+ ", AvgDelVolPer_3day=" + AvgDelVolPer_3day + ", LDAYS=" + LDAYS + ", TID=" + TID + ", MKT_LOT="
				+ MKT_LOT + ", SHRS=" + SHRS + ", PE=" + PE + ", TVOL=" + TVOL + ", DISPTYP=" + DISPTYP + ", im_scid="
				+ im_scid + ", CEPS=" + CEPS + ", DVolAvg10=" + DVolAvg10 + ", pricecurrent=" + pricecurrent
				+ ", priceprevclose=" + priceprevclose + ", symbol=" + symbol + ", company=" + company
				+ ", pricechange=" + pricechange + ", pricepercentchange=" + pricepercentchange + ", lastupd_epoch="
				+ lastupd_epoch + "]";
	}
	
	
}