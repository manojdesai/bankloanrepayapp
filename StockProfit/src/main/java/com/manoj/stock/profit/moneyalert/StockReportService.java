package com.manoj.stock.profit.moneyalert;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Stream;

import org.decimal4j.util.DoubleRounder;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.exc.UnrecognizedPropertyException;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

@Service
public class StockReportService {
	private static ObjectMapper objectMapper = new ObjectMapper();
	private static MyAlerts alerts = null;
	private static OkHttpClient client = new OkHttpClient();

	private static final String template = "Hello, %s!";
	private final AtomicLong counter = new AtomicLong();
	private static final List<Data> stockDataCache = new ArrayList<>();

	static {
		try {

			InputStream configs = Thread.currentThread().getContextClassLoader()
					.getResourceAsStream("com/manoj/stock/profit/moneyalert/myconfigs.json");

			alerts = objectMapper.readValue(configs, MyAlerts.class);
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(
					"Problem encountered while reading json file.**************************************************");
		}
		execute();
	}

	public static void execute() {

		List<Stocks> stocks = alerts.getStocks();

		for (Stocks stock : stocks) {
			Request request = new Request.Builder()
					.url("https://priceapi.moneycontrol.com/pricefeed/nse/equitycash/" + stock.getCode().trim()).get()
					.addHeader("User-Agent",
							"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36")
					.addHeader("Accept", "*/*").addHeader("Cache-Control", "no-cache")
					.addHeader("Host", "priceapi.moneycontrol.com").addHeader("Accept-Encoding", "gzip, deflate")
					.addHeader("Connection", "keep-alive").addHeader("cache-control", "no-cache").build();

			Request requestBSE = new Request.Builder()
					.url("https://priceapi.moneycontrol.com/pricefeed/bse/equitycash/" + stock.getCode().trim()).get()
					.addHeader("User-Agent",
							"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36")
					.addHeader("Accept", "*/*").addHeader("Cache-Control", "no-cache")
					.addHeader("Host", "priceapi.moneycontrol.com").addHeader("Accept-Encoding", "gzip, deflate")
					.addHeader("Connection", "keep-alive").addHeader("cache-control", "no-cache").build();

			try {
				Response response = null;
				if (stock.isIsonlyOnBSE()) {
					response = client.newCall(requestBSE).execute();
				} else {
					response = client.newCall(request).execute();
				}

				String responseBody = response.body().string();
				objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
				Application data = objectMapper.readValue(responseBody, Application.class);
				Data stockdata = data.getData();

				if (stock.getAlertPrice() > 0 && (stockdata.getPricecurrent() == stock.getAlertPrice()
						|| stockdata.getPricecurrent() < stock.getAlertPrice())) {
					System.out.println(
							" ********************** STRONG BUY/SELL PRICE  AT HISTORICAL SELF ANALYSIS BUYING POINT *********  "
									+ stockdata.getSC_FULLNM() + " CURRENT PRICE " + stockdata.getPricecurrent());
				}
				if (stockdata.getAvgDelVolPer_5day() > 65) {
					System.out.println(
							" ********************** BUYING SIGNAL STROOONG ON DELIVER PERCENTAGE IN RECENT WEEK *********  "
									+ stockdata.getSC_FULLNM() + " CURRENT PRICE " + stockdata.getPricecurrent()
									+ " AvgDelVolPer_5Days : " + stockdata.getAvgDelVolPer_5day());
				}

			} catch (Exception e) {
				// TODO: handle exception
			}
		}

		System.out.println(
				"\n *****************'First round Finsh*******************************************************************************");

		for (Stocks stock : stocks) {
			Request request = new Request.Builder()
					.url("https://priceapi.moneycontrol.com/pricefeed/nse/equitycash/" + stock.getCode().trim()).get()
					.addHeader("User-Agent",
							"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36")
					.addHeader("Accept", "*/*").addHeader("Cache-Control", "no-cache")
					.addHeader("Host", "priceapi.moneycontrol.com").addHeader("Accept-Encoding", "gzip, deflate")
					.addHeader("Connection", "keep-alive").addHeader("cache-control", "no-cache").build();

			Request requestBSE = new Request.Builder()
					.url("https://priceapi.moneycontrol.com/pricefeed/bse/equitycash/" + stock.getCode().trim()).get()
					.addHeader("User-Agent",
							"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36")
					.addHeader("Accept", "*/*").addHeader("Cache-Control", "no-cache")
					.addHeader("Host", "priceapi.moneycontrol.com").addHeader("Accept-Encoding", "gzip, deflate")
					.addHeader("Connection", "keep-alive").addHeader("cache-control", "no-cache").build();
			try {
				Response response = null;
				if (stock.isIsonlyOnBSE()) {
					response = client.newCall(requestBSE).execute();
				} else {
					response = client.newCall(request).execute();
				}

				String responseBody = response.body().string();
				objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
				Application data = objectMapper.readValue(responseBody, Application.class);

				Data stockdata = data.getData();
				stockDataCache.add(stockdata);

				double deliveryPercentage = (stockdata.getAvgDelVolPer_5day() + stockdata.getAvgDelVolPer_3day()
						+ stockdata.getAvgDelVolPer_8day()) / 3;
				deliveryPercentage = DoubleRounder.round(deliveryPercentage, 2);

				// Moving Average are Positive
				if ((stockdata.get5DayAvg() > stockdata.get30DayAvg())
						&& (stockdata.get30DayAvg() > stockdata.get50DayAvg())
						&& (stockdata.get50DayAvg() > stockdata.get150DayAvg())) {

					double fairprice = ((stockdata.get5DayAvg() + stockdata.get30DayAvg() + stockdata.get50DayAvg()
							+ stockdata.getOPN() + stockdata.getPriceprevclose()) / 5);

					double dailyFprice = ((stockdata.get5DayAvg() + stockdata.get30DayAvg() + stockdata.get50DayAvg()
							+ stockdata.getOPN() + stockdata.getPriceprevclose()) + stockdata.getHP()
							+ stockdata.getLP() / 5);

					double volumeAvg = (stockdata.getDVolAvg5() + stockdata.getDVolAvg10() + stockdata.getVOL()) / 3;

					double AvgTradedPriceInCr = volumeAvg * dailyFprice / 10000000d;

					double soldPercent = (AvgTradedPriceInCr * 100d) / stockdata.getMKTCAP();

					soldPercent = DoubleRounder.round(soldPercent, 2);

					fairprice = DoubleRounder.round(fairprice, 2);

					if (stockdata.getPricecurrent() < fairprice) {
						System.out
								.println(stockdata.getSC_FULLNM() + " CONSISTENT PERFORMER ENGIBLE FOR INTRADAY BUYING"
										+ fairprice + " Stock Current Price " + stockdata.getPricecurrent()
										+ " WITH RECENT Delivery Perecntage = " + deliveryPercentage);
					} else {
						double above100 = (stockdata.getPricecurrent() * 100) / fairprice - 100;

						above100 = DoubleRounder.round(above100, 2);

						if (above100 < 1.5) {
							System.out.println(stockdata.getSC_FULLNM()
									+ ",  is CONSISTENT PERFORMER share but trading on near higher Price at = "
									+ above100 + "% NearExpensive. DP = " + deliveryPercentage + " FairPrice ="
									+ fairprice + "  SaleBuyIngIntenSity " + soldPercent);
						} else if (above100 > 6.2d) {
							System.out.println(
									stockdata.getSC_FULLNM() + ",  is Good share but trading on higher Price at = "
											+ above100 + "% SellTimeExpensive." + " Fair Price =" + fairprice + " DP = "
											+ deliveryPercentage + "  SaleBuyIngIntenSity " + soldPercent);
						} else {
							System.out.println(stockdata.getSC_FULLNM()
									+ ",  is Good share but trading on higer Price at = " + above100 + "% Expensive."
									+ " Fair Price = " + fairprice + " Current Price = " + stockdata.getPricecurrent()
									+ " DP = " + deliveryPercentage + "  SaleBuyIngIntenSity " + soldPercent);
						}
					}
				}

				// Near Future Averages are Positive BUt long term avg are not positive.
				else if ((stockdata.get5DayAvg() > stockdata.get30DayAvg())
						&& (stockdata.get30DayAvg() > stockdata.get50DayAvg())
						&& (stockdata.get50DayAvg() < stockdata.get150DayAvg())) {

					double fairprice = ((stockdata.get5DayAvg() + stockdata.get30DayAvg() + stockdata.get50DayAvg()
							+ stockdata.getOPN() + stockdata.getPriceprevclose()) / 5);

					fairprice = DoubleRounder.round(fairprice, 2);

					double volumeAvg = (stockdata.getDVolAvg5() + stockdata.getDVolAvg10()) / 2;

					double AvgTradedPriceInCr = volumeAvg * stockdata.getPricecurrent() / 10000000d;

					double breakOutPercent = (AvgTradedPriceInCr * 100d) / stockdata.getMKTCAP();

					breakOutPercent = DoubleRounder.round(breakOutPercent, 2);

					System.out.println(
							stockdata.getSC_FULLNM() + " SHOWING  RECENT BREAK AFTER 5 Month OUT FOR FairPrice Value : "
									+ fairprice + " Breakout Positivity is = " + breakOutPercent + " Vol" + " DP = "
									+ deliveryPercentage + " CP= " + stockdata.getPricecurrent());

					if (stockdata.getPricecurrent() < fairprice) {
						System.out.println(stockdata.getSC_FULLNM()
								+ " WITH RECENT  BREAK OUT BUT UNDER PRESSURE ... FAIR_PRICE = " + fairprice
								+ ", Stock Current Price = " + stockdata.getPricecurrent()
								+ " Breakout Positivity is = " + breakOutPercent + " Vol" + " DP = "
								+ deliveryPercentage);
					}
				}
				// Eligible for Sale
				else {
					double fairprice = ((stockdata.get5DayAvg() + stockdata.get30DayAvg() + stockdata.get50DayAvg()
							+ stockdata.getOPN() + stockdata.getPriceprevclose()) / 5);

					fairprice = DoubleRounder.round(fairprice, 2);

					double volumeAvg = (stockdata.getDVolAvg5() + stockdata.getDVolAvg10()) / 2;

					double AvgTradedPriceInCr = volumeAvg * fairprice / 10000000d;

					double soldPercent = (AvgTradedPriceInCr * 100d) / stockdata.getMKTCAP();

					soldPercent = DoubleRounder.round(soldPercent, 2);

					double sellingPercentage = (stockdata.getPricecurrent() * 100d) / fairprice;

					sellingPercentage = DoubleRounder.round((100 - sellingPercentage), 2);
					if (sellingPercentage > 4.2d && soldPercent > 0.3) {
						System.err.println(
								"Immediately SALE_SALE_SALE .... and Exit.. ' " + stockdata.getSC_FULLNM().trim()
										+ " '  Sale at FairPrice: " + fairprice + ", Stock Current Price = "
										+ stockdata.getPricecurrent() + "  SaleIngPriceDownIntenSity = "
										+ sellingPercentage + " SaleIngVolumeCapitalIntenSity = " + soldPercent);
					} else if (fairprice > stockdata.getPricecurrent()) {
						System.err.println("Stock is Eligible for Sale. ' " + stockdata.getSC_FULLNM().trim()
								+ " '  Sale at FairPrice: " + fairprice + ", Stock Current Price = "
								+ stockdata.getPricecurrent() + "  SaleIngPriceDownIntenSity = " + sellingPercentage
								+ " SaleIngVolumeCapitalIntenSity = " + soldPercent);
					} else {
						deliveryPercentage = (stockdata.getAvgDelVolPer_5day() + stockdata.getAvgDelVolPer_3day()) / 2;
						deliveryPercentage = DoubleRounder.round(deliveryPercentage, 2);
						System.err.println("Careful Stock is Eligible for Sale.  '" + stockdata.getSC_FULLNM().trim()
								+ "' sale at FairPrice: " + fairprice + ", But stock current Price is higher CP= "
								+ stockdata.getPricecurrent() + " DeliveryPercentage: " + deliveryPercentage);
					}
				}
			} catch (UnrecognizedPropertyException e) {
				System.out.println(e.getMessage() + "   ::: " + e.getPropertyName());
			} catch (IOException e) {
				e.printStackTrace();
			}
			try {
				Thread.currentThread().sleep(100);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		System.out.println(
				"\n *****************' Earning Money is Very hard and difficult, so save Money, Spend it wisely, Invest it Carefully. if you respect money, it will respect you.' ***************************** \n");

	}

	public Greeting getGreeting(String name) {
		return new Greeting(counter.incrementAndGet(), String.format(template, name));
	}

	public Data getStockData(String string) {
		System.out.println("getStockData Size of cahce is  =" + stockDataCache.size());

		return stockDataCache.stream()
				.filter(data -> (data.getSC_FULLNM().equals(string) || data.getSymbol().equals(string))).findFirst()
				.orElse(null);

	}

}
