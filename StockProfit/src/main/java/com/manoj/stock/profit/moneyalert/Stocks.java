package com.manoj.stock.profit.moneyalert;

import com.fasterxml.jackson.annotation.JsonGetter;

public class Stocks {
	
	private String code;

	private double alertPrice;

	private String name;

	private boolean isonlyOnBSE;

	@JsonGetter("code")
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	@JsonGetter("alertPrice")
	public double getAlertPrice() {
		return alertPrice;
	}

	public void setAlertPrice(double alertPrice) {
		this.alertPrice = alertPrice;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "ClassPojo [code = " + code + ", alertPrice = " + alertPrice + ", name = " + name + "]";
	}

	public boolean isIsonlyOnBSE() {
		return isonlyOnBSE;
	}

	public void setIsonlyOnBSE(boolean isonlyOnBSE) {
		this.isonlyOnBSE = isonlyOnBSE;
	}
}