package com.manoj.stock.profit.moneyalert;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;

public class DataDeserializer extends JsonDeserializer<Data> {

	@Override
	public Data deserialize(JsonParser jp, DeserializationContext ctxt) throws IOException, JsonProcessingException {
		 ObjectCodec oc = jp.getCodec();
		 JsonNode node = oc.readTree(jp);
		 System.out.println("deserialize  +++++++"+jp.readValueAs(Application.class));
		 return null;
	}
}
