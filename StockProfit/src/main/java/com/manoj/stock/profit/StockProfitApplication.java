package com.manoj.stock.profit;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = "com.manoj.stock.*")

public class StockProfitApplication {

	public static void main(String[] args) {
		SpringApplication.run(StockProfitApplication.class, args);
		
		System.out.println("Spring boot started");
	}

}
