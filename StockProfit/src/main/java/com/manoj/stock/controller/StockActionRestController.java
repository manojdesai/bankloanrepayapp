package com.manoj.stock.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.manoj.stock.profit.moneyalert.Data;
import com.manoj.stock.profit.moneyalert.Greeting;
import com.manoj.stock.profit.moneyalert.StockReportService;

import io.micrometer.core.instrument.Timer;

@RestController
public class StockActionRestController {

	@Autowired
	private StockReportService service;
	private Timer stockactioncontrollerMethodTimer;

	/*
	 * @GetMapping("/greeting") public Greeting greeting(@RequestParam(value =
	 * "name", defaultValue = "World") String name) { return
	 * service.getGreeting(name); }
	 */

	@GetMapping("/greeting")
	public Greeting greeting() {
		return service.getGreeting("TRY WITHOUT Params");
	}

	@GetMapping("/getstcokdata/{stockName}")
	public @ResponseBody Data getStockData(@PathVariable("stockName") String stockName) {
		// stockactioncontrollerMethodTimer.record(() -> dontCareAboutReturnValue());
		return service.getStockData(stockName);
	}

}