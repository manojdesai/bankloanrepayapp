package com.desai.bank.dto;

import java.time.LocalDateTime;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.springframework.format.annotation.DateTimeFormat;


/**
 * @author inmdesai01
 *
 */
public class LoanRequirementsDTO {

	@NotNull(message = "loanAmount can't be Null")
	@Min(0)
	private Double loanAmount;

	@NotNull(message = "nominalRate can't be blank is input.")
	@Min(0)
	private Double nominalRate;

	private int duration;

	@DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss'Z'")
	@NotNull(message = "startDate can't be Null")
	private LocalDateTime startDate;

	public LoanRequirementsDTO(Double loanAmount, Double nominalRate, int duration, LocalDateTime startDate) {
		super();
		this.loanAmount = loanAmount;
		this.nominalRate = nominalRate;
		this.duration = duration;
		this.startDate = startDate;
	}

	public Double getLoanAmount() {
		return loanAmount;
	}

	public void setLoanAmount(Double loanAmount) {
		this.loanAmount = loanAmount;
	}

	public Double getNominalRate() {
		return nominalRate;
	}

	public void setNominalRate(Double nominalRate) {
		this.nominalRate = nominalRate;
	}

	public int getDuration() {
		return duration;
	}

	public void setDuration(int duration) {
		this.duration = duration;
	}

	public LocalDateTime getStartDate() {
		return startDate;
	}

	public void setStartDate(LocalDateTime startDate) {
		this.startDate = startDate;
	}

	@Override
	public String toString() {
		return "LoanRequirementsDTO [loanAmount=" + loanAmount + ", nominalRate=" + nominalRate + ", duration="
				+ duration + ", startDate=" + startDate + "]";
	}

}
