package com.desai.bank.dto;

import java.util.ArrayList;
import java.util.List;

/**
 * @author inmdesai01
 *
 */
public class RePaymentsDTO {

	List<MonthlyAmortizationViewDTO> borrowerPayments = new ArrayList<MonthlyAmortizationViewDTO>();

	public List<MonthlyAmortizationViewDTO> getBorrowerPayments() {
		return borrowerPayments;
	}

	public void setBorrowerPayments(List<MonthlyAmortizationViewDTO> borrowerPayments) {
		this.borrowerPayments = borrowerPayments;
	}
}
