package com.desai.bank;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LoanPlanApplication {

	public static void main(String[] args) {
		SpringApplication.run(LoanPlanApplication.class, args);
	}

}
