package com.desai.bank.service;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.Month;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.desai.bank.dto.LoanRequirementsDTO;
import com.desai.bank.dto.MonthlyAmortizationCalcuationDTO;
import com.desai.bank.dto.MonthlyAmortizationViewDTO;
import com.desai.bank.dto.RePaymentsDTO;
import com.desai.bank.exception.DTOConvertException;
import com.desai.bank.exception.LoanRepayAppBaseExecption;
import com.desai.bank.util.BankLoanRepayAppUtil;

/**
 * @author inmdesai01
 *
 */

@Service
public class LoanRepayPlanServiceImpl implements LoanRepayPlanService {

	Logger logger = LoggerFactory.getLogger(LoanRepayPlanServiceImpl.class);

	public RePaymentsDTO generatePlan(LoanRequirementsDTO inputDTO) {
		ArrayList<MonthlyAmortizationCalcuationDTO> repaycalcaulationDTO = this.generatePlanCalc(inputDTO);

		RePaymentsDTO repaymentResponseDTO = new RePaymentsDTO();
		try {
			ArrayList<MonthlyAmortizationViewDTO> repaycalViewDTO = (ArrayList<MonthlyAmortizationViewDTO>) repaycalcaulationDTO
					.stream().map(BankLoanRepayAppUtil::convertCalculationDTO2ViewDTO).collect(Collectors.toList());
			repaymentResponseDTO.setBorrowerPayments(repaycalViewDTO);
		} catch (DTOConvertException e) {
			logger.error("Exception while generatePlan", e);
			throw e;
		}
		return repaymentResponseDTO;

	}

	public ArrayList<MonthlyAmortizationCalcuationDTO> generatePlanCalc(LoanRequirementsDTO inputDTO)
			throws LoanRepayAppBaseExecption {
		List<LocalDate> months = BankLoanRepayAppUtil.getDaysWithOneMonthDiff(inputDTO.getStartDate().toLocalDate(),
				inputDTO.getDuration());

		// ArrayList<MonthlyAmortizationViewDTO> eachMonthViewValues = new
		// ArrayList<MonthlyAmortizationViewDTO>();

		ArrayList<MonthlyAmortizationCalcuationDTO> eachMonthCalcuationValues = new ArrayList<MonthlyAmortizationCalcuationDTO>();

		double nominalRatePercent = inputDTO.getNominalRate() / 100;
		double monthlyrate = nominalRatePercent / 12;
		int loanDuration = inputDTO.getDuration();

		BigDecimal interestBD = null;
		BigDecimal annuityBD = null;
		BigDecimal principalBD = null;
		BigDecimal borrowerPaymentAmountBD = null;
		BigDecimal remainingOutstandingPrincipalBD = null;

		logger.trace("Start Number of Months loan Tenure :" + months.size());

		for (Iterator iterator = months.iterator(); iterator.hasNext();) {
			LocalDate localDate = (LocalDate) iterator.next();
			MonthlyAmortizationCalcuationDTO calcuationDTO = new MonthlyAmortizationCalcuationDTO();
			calcuationDTO.setDate(localDate.atStartOfDay());

			// Calculation for first Month
			if (eachMonthCalcuationValues.isEmpty()) {

				calcuationDTO.setInitialOutstandingPrincipal(new BigDecimal(inputDTO.getLoanAmount()));

				interestBD = new BigDecimal(monthlyrate).multiply(new BigDecimal(inputDTO.getLoanAmount()));

				calcuationDTO.setInterest(interestBD);

				annuityBD = BankLoanRepayAppUtil.calculateAnnuity(nominalRatePercent, loanDuration,
						inputDTO.getLoanAmount());

				principalBD = annuityBD.subtract(interestBD);

				calcuationDTO.setPrincipal(principalBD);

				borrowerPaymentAmountBD = principalBD.add(interestBD);

				calcuationDTO.setBorrowerPaymentAmount(borrowerPaymentAmountBD);

				remainingOutstandingPrincipalBD = new BigDecimal(inputDTO.getLoanAmount()).subtract(annuityBD);

				calcuationDTO.setRemainingOutstandingPrincipal(remainingOutstandingPrincipalBD);

				eachMonthCalcuationValues.add(calcuationDTO);
			} else {

				MonthlyAmortizationCalcuationDTO previousMonthcalcDTO = eachMonthCalcuationValues
						.get(eachMonthCalcuationValues.size() - 1);

				calcuationDTO.setInitialOutstandingPrincipal(previousMonthcalcDTO.getRemainingOutstandingPrincipal());

				// Every Year in January month EMI ( Annuity ) will be Re-calculated.

				if (localDate.getMonth().equals(Month.JANUARY)) {
					int remainingLoanDuration = loanDuration - eachMonthCalcuationValues.size();

					annuityBD = BankLoanRepayAppUtil.calculateAnnuity(nominalRatePercent, remainingLoanDuration,
							previousMonthcalcDTO.getRemainingOutstandingPrincipal().doubleValue());

					logger.trace("RemainingLoanDuration " + remainingLoanDuration + "  Annuity "
							+ BankLoanRepayAppUtil.roundUpBigDecimalto2Decimal(annuityBD));
				}

				interestBD = new BigDecimal(monthlyrate)
						.multiply(previousMonthcalcDTO.getRemainingOutstandingPrincipal());

				calcuationDTO.setInterest(interestBD);

				// Calculations for Last Month of Tenure
				if (!iterator.hasNext()) {
					logger.trace("Inside Last month calculation " + localDate);
					calcuationDTO.setPrincipal(previousMonthcalcDTO.getRemainingOutstandingPrincipal());
					calcuationDTO.setBorrowerPaymentAmount(
							previousMonthcalcDTO.getRemainingOutstandingPrincipal().add(interestBD));
					calcuationDTO.setRemainingOutstandingPrincipal(BigDecimal.ZERO);
					eachMonthCalcuationValues.add(calcuationDTO);
					break;
				}

				principalBD = annuityBD.subtract(interestBD);

				calcuationDTO.setPrincipal(principalBD);

				borrowerPaymentAmountBD = principalBD.add(interestBD);

				calcuationDTO.setBorrowerPaymentAmount(borrowerPaymentAmountBD);

				remainingOutstandingPrincipalBD = previousMonthcalcDTO.getRemainingOutstandingPrincipal()
						.subtract(annuityBD);

				calcuationDTO.setRemainingOutstandingPrincipal(remainingOutstandingPrincipalBD);

				eachMonthCalcuationValues.add(calcuationDTO);
			}

		}
		return eachMonthCalcuationValues;
	}
}
