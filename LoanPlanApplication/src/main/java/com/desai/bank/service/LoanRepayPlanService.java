package com.desai.bank.service;

import com.desai.bank.dto.LoanRequirementsDTO;
import com.desai.bank.dto.RePaymentsDTO;

/**
 * @author inmdesai01
 * Service Interface for Generate Plan.
 */
public interface LoanRepayPlanService {
	public RePaymentsDTO generatePlan(LoanRequirementsDTO inputDTO);
}
