package com.desai.bank.exception;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

/**
 * @author inmdesai01
 *  Comman Exception handler for Application 
 */

@RestControllerAdvice
public class LoanRepayAppExceptionHandler extends ResponseEntityExceptionHandler {
	@Override
	protected ResponseEntity<Object> handleHttpMessageNotReadable(HttpMessageNotReadableException ex,
			HttpHeaders headers, HttpStatus status, WebRequest request) {
		AppExeptionMessage exMesg = new AppExeptionMessage(status,
				"Request Message Not in readble for ::" + request.toString(), ex);
		return new ResponseEntity<>(exMesg, status);
	}

	@ExceptionHandler(LoanRepayAppBaseExecption.class)
	public ResponseEntity<Object> handleLoanRepayAppBaseExecption(LoanRepayAppBaseExecption ex, WebRequest request) {
		AppExeptionMessage exMesg = new AppExeptionMessage(HttpStatus.INTERNAL_SERVER_ERROR, ex.getMessage(), ex);
		return new ResponseEntity<>(exMesg, HttpStatus.INTERNAL_SERVER_ERROR);

	}

	@Override
	protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
			HttpHeaders headers, HttpStatus status, WebRequest request) {
		AppExeptionMessage exMesg = new AppExeptionMessage(HttpStatus.BAD_REQUEST,
				ex.getBindingResult().getAllErrors().toString());
		return new ResponseEntity<>(exMesg, HttpStatus.BAD_REQUEST);
	}

	/*
	 * @ExceptionHandler(ValidationException.class) public ResponseEntity<Object>
	 * handleConstraintViolation(ValidationException ex, WebRequest request) {
	 * AppExeptionMessage exMesg = new AppExeptionMessage(HttpStatus.BAD_REQUEST,
	 * ex); return new ResponseEntity<>(exMesg, HttpStatus.BAD_REQUEST); }
	 */
}
