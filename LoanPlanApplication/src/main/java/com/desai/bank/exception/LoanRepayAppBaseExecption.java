package com.desai.bank.exception;

/**
 * @author inmdesai01
 *
 */
public class LoanRepayAppBaseExecption extends RuntimeException {
	public LoanRepayAppBaseExecption(String msg, Throwable thb) {
		super(msg, thb);
	}

	public LoanRepayAppBaseExecption(String msg) {
		super(msg);
	}
}
