package com.desai.bank.exception;

import com.desai.bank.dto.MonthlyAmortizationCalcuationDTO;

/**
 * @author inmdesai01
 *
 */
public class DTOConvertException extends LoanRepayAppBaseExecption {
	public DTOConvertException(MonthlyAmortizationCalcuationDTO dto) {
		super("Exception while conversion "+ dto);
	}
}
