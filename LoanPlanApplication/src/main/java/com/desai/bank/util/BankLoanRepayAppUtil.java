package com.desai.bank.util;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.desai.bank.dto.MonthlyAmortizationCalcuationDTO;
import com.desai.bank.dto.MonthlyAmortizationViewDTO;
import com.desai.bank.exception.DTOConvertException;
import com.desai.bank.exception.LoanRepayAppBaseExecption;

/**
 * @author inmdesai01 This class Utility class for various calculation formula,
 *         DTO conversion etc.
 */
public class BankLoanRepayAppUtil {

	private static Logger logger = LoggerFactory.getLogger(BankLoanRepayAppUtil.class);

	public static BigDecimal calculateAnnuity(double nominalRatePercent, int remainingloanRepayMonths,
			double loanAmount) throws ArithmeticException {
		try {
			MathContext mc = new MathContext(5);
			double monthlyrate = nominalRatePercent / 12;
			BigDecimal denominor = new BigDecimal((1 + monthlyrate));
			denominor = denominor.pow(-(remainingloanRepayMonths), mc);
			denominor = new BigDecimal(1).subtract(denominor);
			return new BigDecimal(monthlyrate).multiply(new BigDecimal(loanAmount)).divide(denominor,
					RoundingMode.HALF_UP);
		} catch (ArithmeticException e) {
			throw e;
		}
	}

	public static List<LocalDate> getDaysWithOneMonthDiff(LocalDate start, int months) {
		return Stream.iterate(start, date -> date.plusMonths(1)).limit(months).collect(Collectors.toList());
	}

	public static List<LocalDate> getfirstDayOfEachMonth(LocalDate start, int months) {
		start = start.withDayOfMonth(1); // always considering from start of the month
		return Stream.iterate(start, date -> date.plusMonths(1)).limit(months).collect(Collectors.toList());
	}

	public static double roundUpBigDecimalto2Decimal(BigDecimal number2roundUP) throws LoanRepayAppBaseExecption {
		if (number2roundUP != null) {
			return number2roundUP.setScale(2, RoundingMode.HALF_UP).doubleValue();
		} else if (number2roundUP.ZERO.equals(BigDecimal.ZERO)) {

			return 0;
		} else {
			throw new LoanRepayAppBaseExecption("Trying to Round Null BigDecimal.");
		}
	}

	public static MonthlyAmortizationViewDTO convertCalculationDTO2ViewDTO(MonthlyAmortizationCalcuationDTO calcDTO)
			throws DTOConvertException {
		MonthlyAmortizationViewDTO viewDTO = new MonthlyAmortizationViewDTO();
		try {
			viewDTO.setBorrowerPaymentAmount(roundUpBigDecimalto2Decimal(calcDTO.getBorrowerPaymentAmount()));
			viewDTO.setInitialOutstandingPrincipal(
					roundUpBigDecimalto2Decimal(calcDTO.getInitialOutstandingPrincipal()));
			viewDTO.setInterest(roundUpBigDecimalto2Decimal(calcDTO.getInterest()));
			viewDTO.setPrincipal(roundUpBigDecimalto2Decimal(calcDTO.getPrincipal()));
			viewDTO.setRemainingOutstandingPrincipal(
					roundUpBigDecimalto2Decimal(calcDTO.getRemainingOutstandingPrincipal()));
			viewDTO.setDate(calcDTO.getDate());
		} catch (Exception e) {
			logger.error("Exception while conversion of calcDTO2ViewDTO || " + e.getMessage(), e);
			throw new DTOConvertException(calcDTO);
		}
		return viewDTO;

	}

}
