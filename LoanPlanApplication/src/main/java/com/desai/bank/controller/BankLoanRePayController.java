package com.desai.bank.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.desai.bank.dto.LoanRequirementsDTO;
import com.desai.bank.dto.RePaymentsDTO;
import com.desai.bank.service.LoanRepayPlanServiceImpl;

@RestController
public class BankLoanRePayController {
	@Autowired
	public LoanRepayPlanServiceImpl loanrePayService;

	@PostMapping("/generate-plan")
	public RePaymentsDTO generateLoanRepayPlan(@Valid @RequestBody LoanRequirementsDTO inputDTO ) {		
		//System.out.println(ex.getLocalizedMessage());
		return loanrePayService.generatePlan(inputDTO);
		
	}

}
