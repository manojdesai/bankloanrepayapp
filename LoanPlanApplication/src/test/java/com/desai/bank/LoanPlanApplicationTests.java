package com.desai.bank;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.time.LocalDateTime;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.desai.bank.dto.LoanRequirementsDTO;
import com.fasterxml.jackson.databind.ObjectMapper;

@SpringBootTest
@RunWith(SpringRunner.class)

@AutoConfigureMockMvc
class LoanPlanApplicationTests {

	@Autowired
	private MockMvc mvc;

	@Test
	void contextLoads() {
	}

	@Test
	public void generateLoanRepayPlan_whenInCorrectInput_thenFail() throws Exception {
		this.mvc.perform(MockMvcRequestBuilders.post("/generate-plan")
				.content(asJsonString(new LoanRequirementsDTO(5000d, null, 10, LocalDateTime.now())))
				.contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON)).andExpect(status().isBadRequest());

	}
	
	public void generateLoanRepayPlan_whenCorrectInput_thenSuccess() throws Exception {
		this.mvc.perform(MockMvcRequestBuilders.post("/generate-plan")
				.content(asJsonString(new LoanRequirementsDTO(5000d, 5.5, 10, LocalDateTime.now())))
				.contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk());

	}

	public static String asJsonString(final Object obj) {
		try {
			return new ObjectMapper().writeValueAsString(obj);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
}
